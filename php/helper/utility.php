<?php

    $prefix = "/PinterestDemo";
        
    function getConnection() {
        $servername = "localhost";
        $username = "root";
        $password = "";
        $db = "PinterestDemo";

        // Create connection
        $conn = mysqli_connect($servername, $username, $password, $db);
        
        mysqli_set_charset($conn, "utf8");
        
        return $conn;
    }
    
    function isSetAndNotEmpty($value) {
        return isset($value) && !empty($value);
    }
    
    function formatDateTimeString($datetimeVal) {
        $dateTimeObj = new DateTime($datetimeVal);
        return date_format($dateTimeObj, 'd/m/Y H:i:s');
    }
    
    $ROLE_ADMIN = "Admin";
    $ROLE_USER = "User";
    
    session_start();
?>


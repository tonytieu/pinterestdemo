<?PHP 
    include './template/_headerPartial.php';
?>


<?php
    
    $classCode = "C1606L";
//    echo "test $classCode test";
//    echo "<br />";
//    echo '123 ' . $classCode . ' 4546';
    
    $postId = null;
        
    // error catching
    if(isset($_REQUEST['postId'])) {
        $postId = $_REQUEST['postId'];
        //echo $postId;
        
        
        //print_r($d);
    }
    
    $accountId = 1;
    $content = null;
    
    $isRefresh = false;
    
    $status = null;
    
    if(isset($_GET["status"])) {
        $status = $_GET["status"];
    }
    
    if($_SERVER["REQUEST_METHOD"] == "POST") {
        if(isset($_POST["comment"])) {
            $content = $_POST["comment"];
        }
        if(isset($_POST["postId"])) {
            $postId = $_POST["postId"];
        }
    }
    
    if($_SERVER["REQUEST_METHOD"] == "GET") {
        if(isset($_GET["comment"])) {
            $content = $_GET["comment"];
        }
        if(isset($_GET["postId"])) {
            $postId = $_GET["postId"];
        }
    }
    
    
    if($postId != null && $content != null) {
        $sql = "insert into Comment(Content, PostId, AccountId) values ('$content',$postId, $accountId)";
        mysqli_multi_query($conn, $sql);
        $isRefresh = true;
    }
    
    
    if($postId != null) {
        
        if($isRefresh) {
            header("Location: /PinterestDemo/php/post_detail.php?postId=" . $postId);
        }
        
        $sql = "select * from Post where PostId = " . $postId;
        $detail = $conn->query($sql);
        $d = $detail->fetch_assoc();
        
        $sql = "select * from Comment where PostId = $postId && ParentCommentId is null";
        $comments = $conn->query($sql);
    }
    
    function renderComments($parentCommentId, $postId) { // O(n^2)
        $conn = getConnection();
        $sql = "select * from Comment Where ParentCommentId = $parentCommentId";
                                        
        $replies = $conn->query($sql);
        echo '<ul class="list-group">';
        while ($reply = $replies->fetch_assoc()) {
            echo '<li class="list-group-item">';
            
            echo $reply["Content"];
            
           // <!--Comment Reply Form-->
            echo '<a class="reply-button" href="#" onclick="showReplyForm(this, event)">Reply</a>';
            echo '<form class="form-inline hide" action="comment_reply.php" method="POST">';

                 echo '<input type="hidden" name="parentCommentId" value="'.$reply["CommentId"].'" />';
                 echo '<input type="hidden" name="postId" value="'.$postId.'" />';

                 echo '<div class="form-group">';
                     echo '<input type="text" class="form-control" id="comment" name="content" placeholder="Reply to '.$reply["Content"].'">';
                 echo '</div>';

                 echo '<button type="submit" class="btn btn-default">Reply</button>';
            echo '</form>';
            // <!--End Comment Reply Form-->
            
            renderComments($reply["CommentId"], $postId);
            
            echo '</li>';
        }
        echo '</ul>';
    }
    
    
?>


<div class="container-fluid text-center">    
    <div class="row content">
        <!--Menu-->
        <?php 
            include './template/_sidebarPartial.php';
        ?>
        <!--End Menu-->

        <!--News Feed-->
        <div class="col-sm-8 text-left">
            
            <?PHp if($status != null && $status == 0) { ?>
                <div class="alert alert-success">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Success!</strong> Comment Deleted
                  </div>
            <?PHP } else if($status != null && $status == 1) { ?>
                <div class="alert alert-danger">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Danger!</strong> Comment Not Deleted
                  </div>
            <?PHP } ?>

            <div class="panel panel-default">
                <div class="panel-heading">

                    <?PHP echo $d['Title']; ?>

                </div>
                <div class="panel-body">
                    <?PHP echo $d['Description']; ?>
                </div>
                <div class="panel-footer">
                    Like
                </div>
                <div class="panel-footer">
                    <!--Comement List-->
                    <ul class="list-group">
                        
                        <?php while($c = $comments->fetch_assoc()) {  ?>
                            <li class="list-group-item">
                                <?PHP echo $c["Content"]; ?>
                                <a href="<?PHP echo $prefix . '/php/comment_delete.php?commentId='.$c["CommentId"].'&postId=' . $postId; ?>" 
                                   class="pull-right"
                                    onclick="onCommentDelete(this, event)"><span class="badge">x</span></a>
                                
                                <!--Comment Reply-->
                                
                                <?PHP
                                    renderComments($c["CommentId"], $postId);
                                ?>
                                
                                <!--End Comment Reply-->
                                
                                
                            </li>
                        <?PHP } ?>
                        <?PHP 
//                            while($c = $comments->fetch_assoc()) {
//                                echo '<li class="list-group-item">';
//                                echo $c["Content"];
//                                echo '<a href="' . 
//                                        $prefix . '/php/comment_delete.php?commentId='.$c["CommentId"].'&postId=' . $postId . 
//                                        '" class="pull-right"><span class="badge">x</span></a>';
//                                echo '';
//                                echo '</li>';
//                            }
                        ?>
                    </ul>
                    <!--End Comment List-->
                    
                    <!--Comment Box-->
                    <form class="form-inline" action="post_detail.php" method="POST">
                        <input type="hidden" name="postId" value="<?PHP echo $postId; ?>" />
                        <div class="form-group">
                            <label for="email">Your comment:</label>
                            <input type="text" class="form-control" id="comment" name="comment">
                        </div>
                        
                        <button type="submit" class="btn btn-default">Submit</button>
                    </form>
                    <!--End Comment Box-->
                </div>
            </div>

        </div>
        <!--End News Feed-->
    </div>
</div>



<script>
    
    let classCode = 'C1606L';
    let s = "123 "+classCode+" 456";
    let s1 = `123 ${classCode} 456`; // backstick - sugar syntax
    //console.log(s1);
    
    setTimeout(
            function(){
                //$(".alert").remove();
                $(".alert").hide('slow', function(){ $(".alert").remove(); });
            }, 
    5000);
    
    function showReplyForm(ele, e) {
        //console.log(e);
        var $currentEle = $(ele);
        console.log($currentEle.siblings("form"));
        
        $currentEle.siblings("form").toggleClass('hide');
        
//        if($currentEle.siblings("form").hasClass("hide")) {
//        
//            $currentEle.siblings("form").removeClass("hide");
//        } else {
//            $currentEle.siblings("form").addClass("hide");
//        }
        
    }
    
    function onCommentDelete(ele, e) {
        
        e.preventDefault();
        
        bootbox.confirm("This is the default confirm!", 
            function(result){
                if(result) {
                    //console.log($(ele).attr("href"));
                    location = $(ele).attr("href");
                }
                //console.log('This was logged in the callback: ' + result); 
            });
        
        
//        var r = confirm("Are u sure to delete this comment");
//        if (r == true) {
//            console.log("yes");
//        } else {
//            e.preventDefault();
//            console.log("no");
//        }
    }
</script>

                
 
<?PHP 
    include './template/_footerPartial.php';
?>

<script src="/PinterestDemo/plugins/bootbox.min.js" type="text/javascript"></script>
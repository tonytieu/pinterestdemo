<?PHP 
    include './template/_headerPartial.php';
    
    session_unset();   
    session_destroy();
    
    //setcookie("userName", "", time() - 86400 * 30 * 30, "/");
    //setcookie("roleName", "", time() - 86400 * 30 * 30, "/");
    setcookie("loggedInUser", "", time() - 86400 * 30 * 30, "/");
    
    header("Location: $prefix/");
    
 ?>
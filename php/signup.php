<?PHP 
    include './template/_headerPartial.php';
    
    $emailError = "";
    $nameError = "";
    $passwordError = "";
    
    $name = "";
    $email = "";
    $pwd = "";
    $confirmpwd = "";
    $roleId = "";
    $isBlocked = true;
    
    if($_SERVER["REQUEST_METHOD"] == "POST") {
        
        if(isSetAndNotEmpty($_POST["email"])) {
            
            $email = $_POST["email"];
        } else {
            
            $emailError = "Email is required!";
        }
        
        if(isSetAndNotEmpty($_POST["name"])) {
            $name = $_POST["name"];
        } else {
            $nameError = "name is required!";
        }
        
        if(isSetAndNotEmpty($_POST["pwd"])) {
            $pwd = $_POST["pwd"];
            
            if(isSetAndNotEmpty($_POST["confirmpwd"]) && $_POST["confirmpwd"] == $pwd) {
                $confirmpwd = $_POST["confirmpwd"];
            } else {
                $passwordError = "Password and ConfirmPassword must be the same";
            }
            
        } else {
            $passwordError = "Password is required!";
        }
        
        if(!isSetAndNotEmpty($emailError) && !isSetAndNotEmpty($nameError) && 
                !isSetAndNotEmpty($passwordError)) {
            // perform db operation
            $roleSql = "select RoleId from Role where Name = '$ROLE_USER'";
            $result = $conn->query($roleSql);
            if($result) {
                $role = $result->fetch_assoc();
                if($role) {
                    $roleId = $role["RoleId"];
                }
            }
            
            $pwd = md5($pwd);
            $sql = "insert Account(Name, Email, Password, isBlocked, RoleId) values ('$name', '$email', '$pwd', $isBlocked, $roleId)";
            $result = $conn->query($sql);
            
            if($result) {
                echo 'Registration succesful';
            }
            // automatically login
            
            // redirect to home page
            header("Location: " . $prefix . "/");
        } else {
            // has error
        }
        
    }
?>

<form class="form-horizontal" method="POST">
    
    <h3>Registration Form</h3>
  <div class="form-group">
    <label class="control-label col-sm-2" for="email">Email:</label>
    <div class="col-sm-10">
        <input type="email" class="form-control" id="email" name="email" placeholder="Enter email" value="<?php echo $email; ?>">
        <span class="label label-danger"><?PHP echo $emailError; ?></span>
    </div>
  </div>
    
  <div class="form-group">
    <label class="control-label col-sm-2" for="name">Name:</label>
    <div class="col-sm-10">
        <input type="text" pattern="[A-Za-z ]+" title="Alphabetical characters" class="form-control" id="name" name="name" placeholder="Enter Name" value="<?php echo $name; ?>">
        <span class="label label-danger"><?PHP echo $nameError; ?></span>
    </div>
  </div>
    
  <div class="form-group">
    <label class="control-label col-sm-2" for="pwd">Password:</label>
    <div class="col-sm-10"> 
        <input type="password" pattern=".{6,}" title="Min length is 6" class="form-control" id="pwd" name="pwd" placeholder="Enter password" value="<?php echo $pwd; ?>">
        <span class="label label-danger"><?PHP echo $passwordError; ?></span>
    </div>
  </div>
    
   <div class="form-group">
    <label class="control-label col-sm-2" for="pwd">Confirm Password:</label>
    <div class="col-sm-10"> 
        <input type="password" pattern=".{6,}" title="Min length is 6" class="form-control" id="confirmpwd" name="confirmpwd" placeholder="" value="<?php echo $confirmpwd; ?>">
    </div>
  </div>

  <div class="form-group"> 
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-default">Register</button>
    </div>
  </div>
    
</form>


<?PHP 
    include './template/_footerPartial.php';
?>
<?PHP
include './template/_headerPartial.php';
$userArr = null;

if (isset($_SESSION["loggedInUser"])) {
    $userArr = $_SESSION["loggedInUser"];
} else if (isset($_COOKIE["loggedInUser"])) {
    $userArr = unserialize($_COOKIE["loggedInUser"]);
}

if (!$userArr) {
    die("No profile to display");
}

//print_r($userArr);

    $imgFolder = $prefix . "/img/";

    $accountId = $userArr["accountId"];
    $name = null;
    $facebookUrl = null;
    $avatar = null;
    $avatarUrl = null;// existing avartar
    $gender = null;
    $interests = array();
    $updatedOn = null;
    $fileName = null;

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    
    print_r($_POST);



    if (isset($_POST["Name"])) {
        $name = $_POST["Name"];
    }

    if (isset($_POST["FacebookUrl"])) {
        $facebookUrl = $_POST["FacebookUrl"];
    }
    
    if (isset($_POST["Gender"])) {
        $gender = $_POST["Gender"];
    }
    
    if (isset($_POST["Interests"])) {
        $interests = $_POST["Interests"];
    }
    
    if (isset($_POST["AvatarUrl"])) {
        $avatarUrl = $_POST["AvatarUrl"];
    }
    
    if(!empty($_FILES["Avatar"]["name"])) {

        $fileName = $_FILES["Avatar"]["name"];
        $fileType = $_FILES["Avatar"]["type"];
        $tempPath = $_FILES["Avatar"]["tmp_name"];
        
        $avatar = $imgFolder . $fileName;
        
        // validate
        $uploadResult = move_uploaded_file($tempPath, $_SERVER["DOCUMENT_ROOT"] . $avatar);
        
        if($uploadResult) {
            
            if(file_exists($_SERVER["DOCUMENT_ROOT"] . $avatarUrl)) {
                unlink($_SERVER["DOCUMENT_ROOT"] . $avatarUrl); // /Pinteerestdemo/img/adfadsf
            }
            echo 'UPload successfaully';
            
        } else {
            echo "Umable to upload file";
        }
    }


    if($avatar) {
        $sql = "update Account
                    set Name = '$name', FacebookUrl = '$facebookUrl', Gender = '$gender', UpdatedOn = now(), Avartar = '$avatar'
                    where AccountId = $accountId";
    } else {
        $sql = "update Account
                set Name = '$name', FacebookUrl = '$facebookUrl', Gender = '$gender', UpdatedOn = now()
                where AccountId = $accountId";
    }
    $result = $conn->query($sql);
    
    $interestDelete = "delete from Interest_Account where AccountId = $accountId";
    $conn->query($interestDelete);
    
    foreach ($interests as $interestId) {
        
        $interstsInsert = "insert into Interest_Account(InterestId, AccountId) values ($interestId, $accountId)";
        
        $conn->query($interstsInsert);
    }
    

    if ($result) {
        echo 'Updated Profile';
    } else {
        echo 'Cant upadte profile';
    }
}

    $genderSql = "select * from Ref_Gender";
    $genderRrsult = $conn->query($genderSql);
    
    $interestSql = "select * from Interest";
    $interestResult = $conn->query($interestSql);
    
    $accountRecordd = array();
    
    $accountSql = "select * from Account where AccountId = $accountId";
    
    $accountRecordResult = $conn->query($accountSql);
    
    if($accountRecordResult) {
        $accountRecordd = $accountRecordResult->fetch_assoc();
        
        $name = $accountRecordd["Name"];
        $facebookUrl = $accountRecordd["FacebookURL"];
        $avatar = $accountRecordd["Avartar"];
        $gender = $accountRecordd["Gender"];
        $updatedOn = $accountRecordd["UpdatedOn"];
        
        $interestsSelect = "select i.InterestId, i.Name from Account a
            join Interest_Account ia
                    on a.AccountId = ia.AccountId
            join Interest i
                    on ia.InterestId = i.InterestId";
        
        $interstsSelectResult = $conn->query($interestsSelect);
        while($userInterst = $interstsSelectResult->fetch_assoc()) {
            array_push($interests, $userInterst["InterestId"]);
        }
    }
?>


<h1>Hello <?php echo $userArr["userName"]; ?></h1>

<form class="form-horizontal" method="POST" action="" enctype="multipart/form-data">
    <div class="form-group">
        <label class="control-label col-sm-2">UpdatedOn:</label>
        <div class="col-sm-10">
            <input type="text" disabled="" class="form-control" placeholder="" value="<?PHP echo formatDateTimeString($updatedOn); ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="name">Name:</label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="Name" name="Name" placeholder="" value="<?PHP echo $name; ?>">
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-sm-2" for="FacebookUrl">FacebookUrl:</label>
        <div class="col-sm-10"> 
            <input type="text" class="form-control" id="FacebookUrl" name="FacebookUrl" placeholder=""  value="<?PHP echo $facebookUrl; ?>">
        </div>
    </div>
    
    <div class="form-group">
        <label class="control-label col-sm-2" for="FacebookUrl">Gender:</label>
        <div class="radio">
            <?PHP while( $gen = $genderRrsult->fetch_assoc() ) { ?>
            <?PHP 
                $currentGender = $gen["Name"];
                $isChecked = "";
                
                if($gender == $currentGender) {
                    $isChecked = 'checked=""';
                }
            ?>
                <label>
                    <input type="radio" name="Gender" value="<?PHP echo $currentGender; ?>" <?PHP echo $isChecked; ?> > 
                    <?PHP echo $currentGender; ?>
                </label>
            <?PHP } ?>
        </div>
    </div>

    <div class="form-group">
        <label class="control-label col-sm-2" for="FacebookUrl">Interests:</label>
        <div class="checkbox">
            <?PHP while( $interest = $interestResult->fetch_assoc() ) { ?>
            <?PHP 
                $currentInterest = $interest["Name"];
                $currentInterestId = $interest["InterestId"];
                $isChecked = "";
                
                if(in_array($currentInterestId, $interests)) {
                    $isChecked = 'checked=""';
                }
            ?>
                <label>
                    <input type="checkbox" name="Interests[]" value="<?PHP echo $interest["InterestId"] ?>" <?PHP echo $isChecked; ?> > 
                    <?PHP echo $interest["Name"] ?> 
                </label>
            <?PHP } ?>
        </div>

    </div>

    <div class="form-group">
        <label class="control-label col-sm-2" for="Avatar">Avatar:</label>
        <div class="col-sm-10">
            <input type="file" class="form-control" id="Avatar" name="Avatar" placeholder="" accept="image/*">
            <input type="file" class="form-control" id="Avatar" name="Avatar2" placeholder="" accept="image/*">
            <input type="file" class="form-control" id="Avatar" name="Avatar3" placeholder="" accept="image/*">
            
            <input type="hidden" name="AvatarUrl" value="<?PHp echo $avatar; ?>" />
            <img src="<?PHP echo $avatar; ?>" />
        </div>
    </div>

    <div class="form-group"> 
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-default">Submit</button>
        </div>
    </div>
</form>

<?PHP
include './template/_footerPartial.php';
?>
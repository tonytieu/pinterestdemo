<?php 
    include './helper/utility.php';

    $conn = getConnection();
?>

<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor. SEO-friendly
-->
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Web programming with php">
        <meta name="keywords" content="image,pinterest,aptech,c160l">
        <meta name="author" content="C160l">
        <title>Home Page</title>

        <!--Third party-->
        <link rel="stylesheet" href="<?PHP echo $prefix; ?>/plugins/bootstrap/css/bootstrap.min.css">

        <!--End Third Party-->

        <!--Custom-->
        <link rel="stylesheet" href="<?PHP ECHO $prefix; ?>/css/style.css">
        <!--End Custom-->

        <style>

        </style>
    </head>
    <body>
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>                        
                    </button>
                    <a class="navbar-brand" href="#">Logo</a>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav">
                        <li class="active"><a href="<?PHP echo $prefix; ?>/">Home</a></li>
                        <li><a href="#">About</a></li>
                        <li><a href="#">Projects</a></li>
                        <li><a href="#">Contact</a></li>
                        
                        <!--Admin Section-->
                        <?PHP if( (isset($_SESSION["loggedInUser"]["roleName"]) && $_SESSION["loggedInUser"]["roleName"] == $ROLE_ADMIN) || 
                                ( isset($_COOKIE["loggedInUser"]) &&  unserialize($_COOKIE["loggedInUser"])["roleName"] == $ROLE_ADMIN ) ) { ?>
                            <li><a href="<?PHP echo $prefix; ?>/php/admin_posts.php">Posts</a></li>
                        <?PHP }?>
                        <!--End Admin Section-->
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        
                        <?PHP if(!isset($_SESSION["loggedInUser"]) && !isset($_COOKIE["loggedInUser"])) { ?>
                        
                        <li><a href="<?PHP echo $prefix; ?>/php/signup.php"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                        <li><a href="<?PHP echo $prefix; ?>/php/login.php"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                        
                        <?PHP } else { ?>
                        
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                                <?PHP if(isset($_SESSION["loggedInUser"]["avartar"])) { ?>
                                    <img style="width: 25px;" alt="avatar" src="<?php echo $_SESSION["loggedInUser"]["avartar"]; ?>" />
                                <?PHP } else if( isset(unserialize($_COOKIE["loggedInUser"])["avartar"]) ) { ?>
                                    <img style="width: 25px;" alt="avatar" src="<?php echo unserialize($_COOKIE["loggedInUser"])["avartar"]; ?>" />
                                <?PHP } else { ?>
                                    <img style="width: 25px;" alt="avatar" src="https://cdn2.iconfinder.com/data/icons/website-icons/512/User_Avatar-512.png" />
                                <?PHP } ?>
                                
                                <?PHP 
                                    if(isset($_SESSION["loggedInUser"]["userName"])) {
                                        echo $_SESSION["loggedInUser"]["userName"];
                                    } else if(isset(unserialize($_COOKIE["loggedInUser"])["userName"])) {
                                        echo unserialize($_COOKIE["loggedInUser"])["userName"];
                                    }
                                ?>
                            <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                              <li><a href="<?PHP echo $prefix; ?>/php/profile.php">Profile</a></li>
                              <li><a href="<?PHP echo $prefix; ?>/php/logout.php">Logout</a></li>
                            </ul>
                          </li>
                          
                          <?PHP } ?>
                    </ul>
                </div>
            </div>
        </nav>
<?php

    include './helper/utility.php';
    
    $commentId = null;
    $postId = null;
    
    $conn = getConnection();
    
    if(isset($_REQUEST["postId"])) {
        $postId = $_REQUEST["postId"];
    }
    
    if(isset($_GET["commentId"])) {
        $commentId = $_GET["commentId"];
    }
    
    if($commentId && $postId) {
        $sql = "delete from Comment where CommentId = $commentId";
        
        if ( mysqli_query($conn, $sql)) {
            header("Location: $prefix/php/post_detail.php?postId=$postId&status=0");
        } else {
            header("Location: $prefix/php/post_detail.php?postId=$postId&status=1");
        }
    } else {
        echo 'CommentId or PostId is null';
    }
    
?>

